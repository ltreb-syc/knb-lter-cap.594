# overview

1. Five to eight cross-sectional transect sites were designated at the beginning of each growing season within each stream reach. Transect locations and widths were held consistent throughout each year using markers, but not necessarily between years. Transect locations were measured and labeled in meters downstream of the nutrient injection site (0m) and spaced 20-50 m apart, depending on the total length of the reach. 

2. A measuring tape was stretched from a marker on left bank to marker on right bank (marker locations consistent through growing period). Measuring tape was tied at 1 m. All measurements presented in data sheet are patch lengths less 1 m. 

3. Measurer walked through the stream cross-section along downstream side of measuring tape, looking 0.5 m upstream of tape for intersections of algal, plant, and sediment patches. 

4. Lengths of patches measuring ≥20 centimeter across the measuring tape were recorded.

# Treatment of Wetland Plants on Transects - detailed protocol

## Author: Xiaoli Dong, modified by N. Grimm

## Goals of density measurement along transects

1.  To record changes in plant density and height for different species over time;

2.  To estimate plant density at plant patch scale;

3.  To understand the relationship between hydrological conditions and seasonal growth and wetland density changes.

## Overview:

The density varies between species, and even within species (at the same time of the year), at different locations; for example, in 2011, density in July for different species was:

-   *Schoenoplectus americanus* (SCAM): 484±304 /m^2^

-   *Paspalum disticum* (PADI): 972±825 /m^2^

-   *Equisetum laevigatum* (EQLA): 441±252/m^2^

-   *Typha dominguensis* (TYDO): 45±23/m^2^

-   *Juncus torreyi* (JUTO): 51±16/m^2^

(Therefore, we will assign a density class to each species encountered on the transect that reflects the above preliminary data: low (lowest quartile), medium (middle two quartiles), and high (highest quartile). Early in the season, all patches may fall into the "low" category and late in the season, many may be in the "high" category.) NOTE: These classes will be assigned a posteriori; density and plant height are to be measured each time when transects are done in the reach.

## Procedure

1.  This will be done while we survey the cross-stream transects in the three wetland reaches. Do plant density estimation within 1 m either side of the transect, wherever the patch is found. Do plant height measurement for 3 individuals of each plant species encountered on the transect. Measure the height of the central stem from base to tip.

2.  For SCAM, PADI, EQLA, use an appropriately sized quadrat to count stems. Generally, you will use a 10cmx10cm quadrat for EQLA, and a 25cmx25cm quadrat for SCAM and PADI. Write down all densities as count/length of quadrat side (e.g., 25/10 or 13/25). Count stems from where they emerge from the ground.

3.  For TYDO and JUTO, in many cases the density is low, so a small quadrat (even 1m\*1m) won't capture the representative density. For example, the patch size of JUTO is usually very small, or it is rectangular in shape with width \<1m but length \>1m. There are two options for these species.

    a.  Measure the area of the entire patch and count all stems. Make this area rectangular and include all plants. Write down as count/area (e.g., 6/4 m^2^).

    b.  If the patch is too big to count all plants (e.g., with TYDO), use two 50 cmx50cm quadrats linked together to count stems (i.e., a 0.5 m^2^ area). Write down as count/area (e.g., 6/0.5 m^2^)

4.  While running the transect, we will record the water depth, and wet/dry information, this information will be useful in understanding the trajectory of changes in density: whether the density increases fast in wet patches, or when a patch is closer to water etc.
