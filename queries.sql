
# transects

-- postgres was not available programatically at the time of construction so
-- the data were harvested via ssh and harvested as files

## wetted extents

SELECT
  sites.site_code,
  reaches.reach_code,
  reach_distances.distance,
  reach_transects.date,
  reach_transects.note_transect,
  measure_types.measure_code,
  rwe.extent_measure AS measure_value,
  rwe.note_reach_extent
FROM
  reach_transects
JOIN sites ON sites.id = reach_transects.site_id
JOIN reaches ON reaches.id = reach_transects.reach_id
JOIN reach_distances ON reach_distances.id = reach_transects.distance_id
JOIN reach_wet_extents rwe ON rwe.transect_id = reach_transects.id
JOIN measure_types ON measure_types.id = rwe.measure_type_id
ORDER BY
  date, site_code, reach_code, distance
;

## wetland plants

SELECT
  sites.site_code,
  reaches.reach_code,
  reach_distances.distance,
  reach_transects.date,
  reach_transects.note_transect,
  plant_taxa.plant_name,
  reach_plants.note_reach_plant,
  measure_types.measure_code,
  rpm.measure_value,
  rpm.note_rpm AS note_measure
FROM
  reach_transects
JOIN sites ON sites.id = reach_transects.site_id
JOIN reaches ON reaches.id = reach_transects.reach_id
JOIN reach_distances ON reach_distances.id = reach_transects.distance_id
JOIN reach_plants ON reach_plants.transect_id = reach_transects.id
JOIN reach_plant_measures rpm ON rpm.reach_plant_id = reach_plants.id
JOIN measure_types ON measure_types.id = rpm.measure_type_id
JOIN plant_taxa ON plant_taxa.id = reach_plants.plant_taxon_id
ORDER BY
  date, site_code, reach_code, distance
;

\COPY (SELECT sites.site_code, reaches.reach_code, reach_distances.distance, reach_transects.date, reach_transects.note_transect, plant_taxa.plant_name, reach_plants.note_reach_plant, measure_types.measure_code, rpm.measure_value, rpm.note_rpm AS note_measure FROM reach_transects JOIN sites ON sites.id = reach_transects.site_id JOIN reaches ON reaches.id = reach_transects.reach_id JOIN reach_distances ON reach_distances.id = reach_transects.distance_id JOIN reach_plants ON reach_plants.transect_id = reach_transects.id JOIN reach_plant_measures rpm ON rpm.reach_plant_id = reach_plants.id JOIN measure_types ON measure_types.id = rpm.measure_type_id JOIN plant_taxa ON plant_taxa.id = reach_plants.plant_taxon_id ORDER BY date, site_code, reach_code, distance) TO 'wetland_plants.csv' DELIMITER ',' CSV HEADER ;

## patch types

SELECT
  sites.site_code,
  reaches.reach_code,
  reach_distances.distance,
  reach_transects.date,
  reach_transects.note_transect,
  reach_patches.length,
  reach_patches.note_reach_patch,
  patch_codes.patch_code
FROM
  reach_transects
JOIN sites ON sites.id = reach_transects.site_id
JOIN reaches ON reaches.id = reach_transects.reach_id
JOIN reach_distances ON reach_distances.id = reach_transects.distance_id
JOIN reach_patches ON reach_patches.transect_id = reach_transects.id
JOIN reach_patch_types rpt ON rpt.reach_patch_id = reach_patches.id
JOIN patch_codes ON patch_codes.id = rpt.patch_code_id
ORDER BY
  date, site_code, reach_code, distance
;

SELECT sites.site_code, reaches.reach_code, reach_distances.distance, reach_transects.date, reach_transects.note_transect, reach_patches.length, reach_patches.note_reach_patch, patch_codes.patch_code FROM reach_transects JOIN sites ON sites.id = reach_transects.site_id JOIN reaches ON reaches.id = reach_transects.reach_id JOIN reach_distances ON reach_distances.id = reach_transects.distance_id JOIN reach_patches ON reach_patches.transect_id = reach_transects.id JOIN reach_patch_types rpt ON rpt.reach_patch_id = reach_patches.id JOIN patch_codes ON patch_codes.id = rpt.patch_code_id ORDER BY date, site_code, reach_code, distance

\COPY (SELECT sites.site_code, reaches.reach_code, reach_distances.distance, reach_transects.date, reach_transects.note_transect, reach_patches.length, reach_patches.note_reach_patch, patch_codes.patch_code FROM reach_transects JOIN sites ON sites.id = reach_transects.site_id JOIN reaches ON reaches.id = reach_transects.reach_id JOIN reach_distances ON reach_distances.id = reach_transects.distance_id JOIN reach_patches ON reach_patches.transect_id = reach_transects.id JOIN reach_patch_types rpt ON rpt.reach_patch_id = reach_patches.id JOIN patch_codes ON patch_codes.id = rpt.patch_code_id ORDER BY date, site_code, reach_code, distance) TO 'reach_patch_types.csv' DELIMITER ',' CSV HEADER ;

### copy files

scp prod.postgresql:/home/ubuntu/reach_patch_types.csv /home/srearl/Dropbox/development/knb-lter-cap.594/reach_patch_types.csv
scp prod.postgresql:/home/ubuntu/wetland_plants.csv /home/srearl/Dropbox/development/knb-lter-cap.594/wetland_plants.csv
scp prod.postgresql:/home/ubuntu/wetted_extent.csv /home/srearl/Dropbox/development/knb-lter-cap.594/wetted_extent.csv

